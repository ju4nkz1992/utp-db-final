namespace :data do
  desc "Crea los departamentos y ciudades de colombia"
  task :create_departaments => :environment do
    file = File.read(Rails.root.join('lib', 'assets', 'colombia.min.json'))
    departamentos = JSON.parse(file)

    Department.destroy_all()

    departamentos.each do |dep|
      department = Department.create(name: dep["departamento"])
      dep["ciudades"].each do |ciudad|
        City.create(name: ciudad, department: department)
      end
    end
  end

  desc "Crear multas de transito"
  task :traffic_infrations => :environment do
    require 'csv'

    csv_text = File.read(Rails.root.join('lib', 'assets', 'infractions.csv'), :encoding => 'UTF-8')
    csv = CSV.parse(csv_text, :headers => ['article', 'description', 'cost_urban_area'])
    csv.each do |row|
      row['cost_urban_area'] = row['cost_urban_area'].gsub(/\.|\,/, '')
      row['cost_urban_area'] = row['cost_urban_area'].to_i
      row['cost_rural_area'] = row['cost_urban_area'] * 2
      TrafficInfraction.create(row.to_hash)
    end

    TrafficInfraction.pluck(:article)
  end

  desc "crear Agentes de transito"
  task :traffic_officers => :environment do
    [
      {registry_number: '0000001', name: 'Agente nombre 1'},
      {registry_number: '0000002', name: 'Agente nombre 2'},
      {registry_number: '0000003', name: 'Agente nombre 3'},
      {registry_number: '0000004', name: 'Agente nombre 4'},
      {registry_number: '0000005', name: 'Agente nombre 5'},
      {registry_number: '0000006', name: 'Agente nombre 6'},
      {registry_number: '0000007', name: 'Agente nombre 7'},
      {registry_number: '0000008', name: 'Agente nombre 8'},
      {registry_number: '0000009', name: 'Agente nombre 9'},
      {registry_number: '0000010', name: 'Agente nombre 10'},
      {registry_number: '0000011', name: 'Agente nombre 11'},
      {registry_number: '0000012', name: 'Agente nombre 12'},
      {registry_number: '0000013', name: 'Agente nombre 13'}
    ].each do |agent|
      TrafficOfficer.create(agent)
    end
  end

  desc "parametrizar matriculas de automoviles"
  task :parameterize_car_registration => :environment do
    Car.find_each do |car|
      car.parameterize_registration
      car.save
    end
  end
end
