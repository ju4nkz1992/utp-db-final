Rails.application.routes.draw do
  root 'home#index'

  resources :traffic_tickets
  resources :cars
  resources :owners

  resources :departments, only: [] do
    resources :cities, only: [:index]
  end

  resources :automakers, only: [:index, :show, :new, :edit, :create, :update] do
    resources :model_cars
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
