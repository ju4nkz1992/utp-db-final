### UTP Trabajo Final Base de datos 2016 semestre 2

**IS644 Base De Datos I Gr. 2**

**Por:** Juan Camilo Zamora Ortiz

**Cod:** 1053823724

Este projecto podra ser revisado en [bd-final.zaphon.co](http://bd-final.zaphon.co/)

#### Modelo Entidad Relacion:

![Image of Yaktocat](modeloER.png)

### Nota

Todos los nombres de entidades y atributos fueron traducidos al inglés para evitar errores el con la convencion de nombres en rails

#### Mapeo de nombres de relaciones(Tablas)

|Español       |  Ingles            |Español       |  Ingles
|-------------:|:-------------------|-------------:|:-------------------
|Fabricante    |Automaker           |Automovil     |Car
|Departamento  |Department          |Agente        |Traffic Officer
|Ciudad        |City                |Infraccion    |Traffic Infraction
|Modelo        |Model Car           |Multa         |Traffic Ticket
|Propietario   |Owner               |              |


### License

This project is released under the [MIT License](http://www.opensource.org/licenses/MIT).

### Licencia

Este proyecto se publica bajo la [Licencia MIT](http://www.opensource.org/licenses/MIT).