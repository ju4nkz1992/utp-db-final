class CreateCars < ActiveRecord::Migration[5.0]
  def change
    create_table :cars do |t|
      t.string :registration
      t.date :registration_date
      t.references :model_car, foreign_key: true
      t.references :owner, foreign_key: true

      t.timestamps
    end
  end
end
