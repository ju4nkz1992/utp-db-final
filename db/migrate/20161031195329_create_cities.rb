class CreateCities < ActiveRecord::Migration[5.0]
  def change
    create_table :cities do |t|
      t.string :name
      t.references :department

      t.timestamps
    end

    add_foreign_key :cities, :departments
  end
end
