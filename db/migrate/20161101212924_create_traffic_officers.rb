class CreateTrafficOfficers < ActiveRecord::Migration[5.0]
  def change
    create_table :traffic_officers do |t|
      t.string :registry_number
      t.string :name

      t.timestamps
    end
  end
end
