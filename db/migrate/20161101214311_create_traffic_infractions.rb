class CreateTrafficInfractions < ActiveRecord::Migration[5.0]
  def change
    create_table :traffic_infractions do |t|
      t.string :article
      t.decimal :cost_urban_area, :precision => 15, scale: 2
      t.decimal :cost_rural_area, :precision => 15, scale: 2
      t.text :description

      t.timestamps
    end
  end
end
