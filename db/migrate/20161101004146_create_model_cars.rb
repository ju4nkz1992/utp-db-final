class CreateModelCars < ActiveRecord::Migration[5.0]
  def change
    create_table :model_cars do |t|
      t.string :name
      t.integer :year
      t.integer :engine_power
      t.references :automaker, foreign_key: true

      t.timestamps
    end
  end
end
