class CreateOwners < ActiveRecord::Migration[5.0]
  def change
    create_table :owners do |t|
      t.string :nit
      t.string :name
      t.string :last_name
      t.date :birthdate
      t.string :street
      t.string :street_number
      t.references :city, foreign_key: true

      t.timestamps
    end
  end
end
