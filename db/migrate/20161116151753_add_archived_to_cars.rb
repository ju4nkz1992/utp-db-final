class AddArchivedToCars < ActiveRecord::Migration[5.0]
  def change
    add_column :cars, :archived, :boolean, default: false
    add_column :cars, :archive_at, :datetime
  end
end
