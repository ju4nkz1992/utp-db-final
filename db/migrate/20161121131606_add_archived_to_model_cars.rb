class AddArchivedToModelCars < ActiveRecord::Migration[5.0]
  def change
    add_column :model_cars, :archived, :boolean, default: :false
    add_column :model_cars, :archive_at, :datetime
  end
end
