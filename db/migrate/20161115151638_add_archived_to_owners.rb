class AddArchivedToOwners < ActiveRecord::Migration[5.0]
  def change
    add_column :owners, :archived, :boolean, default: false
    add_column :owners, :archive_at, :datetime
  end
end
