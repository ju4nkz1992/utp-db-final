class CreateTrafficTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :traffic_tickets do |t|
      t.references :owner, foreign_key: true
      t.references :car, foreign_key: true
      t.references :traffic_officer, foreign_key: true
      t.references :traffic_infraction, foreign_key: true
      t.references :city, foreign_key: true
      t.date :date
      t.string :address
      t.decimal :cost, :precision => 15, scale: 2

      t.timestamps
    end
  end
end
