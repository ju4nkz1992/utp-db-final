# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161121131606) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "automakers", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cars", force: :cascade do |t|
    t.string   "registration"
    t.date     "registration_date"
    t.integer  "model_car_id"
    t.integer  "owner_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.boolean  "archived",          default: false
    t.datetime "archive_at"
    t.index ["model_car_id"], name: "index_cars_on_model_car_id", using: :btree
    t.index ["owner_id"], name: "index_cars_on_owner_id", using: :btree
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "department_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["department_id"], name: "index_cities_on_department_id", using: :btree
  end

  create_table "departments", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "model_cars", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.integer  "engine_power"
    t.integer  "automaker_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "archived",     default: false
    t.datetime "archive_at"
    t.index ["automaker_id"], name: "index_model_cars_on_automaker_id", using: :btree
  end

  create_table "owners", force: :cascade do |t|
    t.string   "nit"
    t.string   "name"
    t.string   "last_name"
    t.date     "birthdate"
    t.string   "street"
    t.string   "street_number"
    t.integer  "city_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "archived",      default: false
    t.datetime "archive_at"
    t.index ["city_id"], name: "index_owners_on_city_id", using: :btree
  end

  create_table "traffic_infractions", force: :cascade do |t|
    t.string   "article"
    t.decimal  "cost_urban_area", precision: 15, scale: 2
    t.decimal  "cost_rural_area", precision: 15, scale: 2
    t.text     "description"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "traffic_officers", force: :cascade do |t|
    t.string   "registry_number"
    t.string   "name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "traffic_tickets", force: :cascade do |t|
    t.integer  "owner_id"
    t.integer  "car_id"
    t.integer  "traffic_officer_id"
    t.integer  "traffic_infraction_id"
    t.integer  "city_id"
    t.date     "date"
    t.string   "address"
    t.decimal  "cost",                  precision: 15, scale: 2
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.index ["car_id"], name: "index_traffic_tickets_on_car_id", using: :btree
    t.index ["city_id"], name: "index_traffic_tickets_on_city_id", using: :btree
    t.index ["owner_id"], name: "index_traffic_tickets_on_owner_id", using: :btree
    t.index ["traffic_infraction_id"], name: "index_traffic_tickets_on_traffic_infraction_id", using: :btree
    t.index ["traffic_officer_id"], name: "index_traffic_tickets_on_traffic_officer_id", using: :btree
  end

  add_foreign_key "cars", "model_cars"
  add_foreign_key "cars", "owners"
  add_foreign_key "cities", "departments"
  add_foreign_key "model_cars", "automakers"
  add_foreign_key "owners", "cities"
  add_foreign_key "traffic_tickets", "cars"
  add_foreign_key "traffic_tickets", "cities"
  add_foreign_key "traffic_tickets", "owners"
  add_foreign_key "traffic_tickets", "traffic_infractions"
  add_foreign_key "traffic_tickets", "traffic_officers"
end
