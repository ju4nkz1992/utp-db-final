require 'test_helper'

class TrafficOfficersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @traffic_officer = traffic_officers(:one)
  end

  test "should get index" do
    get traffic_officers_url
    assert_response :success
  end

  test "should get new" do
    get new_traffic_officer_url
    assert_response :success
  end

  test "should create traffic_officer" do
    assert_difference('TrafficOfficer.count') do
      post traffic_officers_url, params: { traffic_officer: { name: @traffic_officer.name, registry_number: @traffic_officer.registry_number } }
    end

    assert_redirected_to traffic_officer_url(TrafficOfficer.last)
  end

  test "should show traffic_officer" do
    get traffic_officer_url(@traffic_officer)
    assert_response :success
  end

  test "should get edit" do
    get edit_traffic_officer_url(@traffic_officer)
    assert_response :success
  end

  test "should update traffic_officer" do
    patch traffic_officer_url(@traffic_officer), params: { traffic_officer: { name: @traffic_officer.name, registry_number: @traffic_officer.registry_number } }
    assert_redirected_to traffic_officer_url(@traffic_officer)
  end

  test "should destroy traffic_officer" do
    assert_difference('TrafficOfficer.count', -1) do
      delete traffic_officer_url(@traffic_officer)
    end

    assert_redirected_to traffic_officers_url
  end
end
