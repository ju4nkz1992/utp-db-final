require 'test_helper'

class TrafficInfractionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @traffic_infraction = traffic_infractions(:one)
  end

  test "should get index" do
    get traffic_infractions_url
    assert_response :success
  end

  test "should get new" do
    get new_traffic_infraction_url
    assert_response :success
  end

  test "should create traffic_infraction" do
    assert_difference('TrafficInfraction.count') do
      post traffic_infractions_url, params: { traffic_infraction: { article: @traffic_infraction.article, cost_rural_area: @traffic_infraction.cost_rural_area, cost_urban_area: @traffic_infraction.cost_urban_area, description: @traffic_infraction.description } }
    end

    assert_redirected_to traffic_infraction_url(TrafficInfraction.last)
  end

  test "should show traffic_infraction" do
    get traffic_infraction_url(@traffic_infraction)
    assert_response :success
  end

  test "should get edit" do
    get edit_traffic_infraction_url(@traffic_infraction)
    assert_response :success
  end

  test "should update traffic_infraction" do
    patch traffic_infraction_url(@traffic_infraction), params: { traffic_infraction: { article: @traffic_infraction.article, cost_rural_area: @traffic_infraction.cost_rural_area, cost_urban_area: @traffic_infraction.cost_urban_area, description: @traffic_infraction.description } }
    assert_redirected_to traffic_infraction_url(@traffic_infraction)
  end

  test "should destroy traffic_infraction" do
    assert_difference('TrafficInfraction.count', -1) do
      delete traffic_infraction_url(@traffic_infraction)
    end

    assert_redirected_to traffic_infractions_url
  end
end
