require 'test_helper'

class TrafficTicketsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @traffic_ticket = traffic_tickets(:one)
  end

  test "should get index" do
    get traffic_tickets_url
    assert_response :success
  end

  test "should get new" do
    get new_traffic_ticket_url
    assert_response :success
  end

  test "should create traffic_ticket" do
    assert_difference('TrafficTicket.count') do
      post traffic_tickets_url, params: { traffic_ticket: { address: @traffic_ticket.address, car_id: @traffic_ticket.car_id, city_id: @traffic_ticket.city_id, cost: @traffic_ticket.cost, date: @traffic_ticket.date, owner_id: @traffic_ticket.owner_id, traffic_infraction_id: @traffic_ticket.traffic_infraction_id, traffic_officer_id: @traffic_ticket.traffic_officer_id } }
    end

    assert_redirected_to traffic_ticket_url(TrafficTicket.last)
  end

  test "should show traffic_ticket" do
    get traffic_ticket_url(@traffic_ticket)
    assert_response :success
  end

  test "should get edit" do
    get edit_traffic_ticket_url(@traffic_ticket)
    assert_response :success
  end

  test "should update traffic_ticket" do
    patch traffic_ticket_url(@traffic_ticket), params: { traffic_ticket: { address: @traffic_ticket.address, car_id: @traffic_ticket.car_id, city_id: @traffic_ticket.city_id, cost: @traffic_ticket.cost, date: @traffic_ticket.date, owner_id: @traffic_ticket.owner_id, traffic_infraction_id: @traffic_ticket.traffic_infraction_id, traffic_officer_id: @traffic_ticket.traffic_officer_id } }
    assert_redirected_to traffic_ticket_url(@traffic_ticket)
  end

  test "should destroy traffic_ticket" do
    assert_difference('TrafficTicket.count', -1) do
      delete traffic_ticket_url(@traffic_ticket)
    end

    assert_redirected_to traffic_tickets_url
  end
end
