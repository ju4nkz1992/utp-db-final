class TrafficTicketsController < ApplicationController
  before_action :set_traffic_ticket, only: [:show, :edit, :update, :destroy]

  # GET /traffic_tickets
  # GET /traffic_tickets.json
  def index
    @traffic_tickets = TrafficTicket.search(search_params[:text], search_params[:sort], search_params[:sort_type])
  end

  # GET /traffic_tickets/1
  # GET /traffic_tickets/1.json
  def show
  end

  # GET /traffic_tickets/new
  def new
    @traffic_ticket = TrafficTicket.new
  end

  # GET /traffic_tickets/1/edit
  def edit
  end

  # POST /traffic_tickets
  # POST /traffic_tickets.json
  def create
    @traffic_ticket = TrafficTicket.new(traffic_ticket_params)

    respond_to do |format|
      if @traffic_ticket.save
        format.html { redirect_to @traffic_ticket, notice: 'Traffic ticket was successfully created.' }
        format.json { render :show, locals: {message: 'Multa creada.'}, status: :created, location: @traffic_ticket }
      else
        format.html { render :new }
        format.json { render json: {errors: @traffic_ticket.errors}, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /traffic_tickets/1
  # PATCH/PUT /traffic_tickets/1.json
  def update
    respond_to do |format|
      if @traffic_ticket.update(traffic_ticket_params)
        format.html { redirect_to @traffic_ticket, notice: 'Traffic ticket was successfully updated.' }
        format.json { render :show, locals: {message: 'Multa Actualizada.'}, status: :ok, location: @traffic_ticket }
      else
        format.html { render :edit }
        format.json { render json: {errors: @traffic_ticket.errors}, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /traffic_tickets/1
  # DELETE /traffic_tickets/1.json
  def destroy
    @traffic_ticket.destroy
    respond_to do |format|
      format.html { redirect_to traffic_tickets_url, notice: 'Traffic ticket was successfully destroyed.' }
      format.json { render json: {message: 'Multa Eliminada.'} }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_traffic_ticket
      @traffic_ticket = TrafficTicket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def traffic_ticket_params
      params.require(:traffic_ticket).permit(:owner_id, :car_id, :traffic_officer_id, :traffic_infraction_id, :city_id, :date, :address, :cost)
    end
end
