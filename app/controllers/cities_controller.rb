class CitiesController < ApplicationController
  before_action :set_departament

  # GET /cities
  # GET /cities.json
  def index
    @cities = @departament.cities
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_departament
      @departament = Department.find(params[:department_id])
    end
end
