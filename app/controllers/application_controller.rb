class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale


  private
    def search_params
      params[:search] || {}
    end

    def set_locale
      I18n.locale = 'es-CO'
    end
end
