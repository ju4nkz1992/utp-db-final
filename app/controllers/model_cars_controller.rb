class ModelCarsController < ApplicationController
  before_action :set_automaker
  before_action :set_model_car, only: [:show, :edit, :update, :destroy]

  # GET automakers/:automaker_id/model_cars
  # GET automakers/:automaker_id/model_cars.json
  def index
    @model_cars = ModelCar.search(search_params[:text], search_params[:sort], search_params[:sort_type], automaker: @automaker)
  end

  # GET automakers/:automaker_id/model_cars/1
  # GET automakers/:automaker_id/model_cars/1.json
  def show
  end

  # GET automakers/:automaker_id/model_cars/new
  def new
    @model_car = @automaker.model_cars.new
  end

  # GET automakers/:automaker_id/model_cars/1/edit
  def edit
  end

  # POST automakers/:automaker_id/model_cars
  # POST automakers/:automaker_id/model_cars.json
  def create
    @model_car = @automaker.model_cars.new(model_car_params)

    respond_to do |format|
      if @model_car.save
        format.html { redirect_to automaker_model_car_path(@automaker, @model_car), notice: 'Model car was successfully created.' }
        format.json { render :show, locals: {message: 'Modelo creado.'}, status: :created }
      else
        format.html { render :new }
        format.json { render json: @model_car.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT automakers/:automaker_id/model_cars/1
  # PATCH/PUT automakers/:automaker_id/model_cars/1.json
  def update
    respond_to do |format|
      if @model_car.update(model_car_params)
        format.html { redirect_to automaker_model_car_path(@automaker, @model_car), notice: 'Model car was successfully updated.' }
        format.json { render :show, locals: {message: 'Modelo Actualizado.'}, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @model_car.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE automakers/:automaker_id/model_cars/1
  # DELETE automakers/:automaker_id/model_cars/1.json
  def destroy
    @model_car.destroy
    respond_to do |format|
      format.html { redirect_to automaker_model_cars_url, notice: 'Model car was successfully destroyed.' }
      format.json { render json: {message: 'Modelo Eliminado.'} }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_automaker
      @automaker = Automaker.find(params[:automaker_id])
    end

    def set_model_car
      @model_car = @automaker.model_cars.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def model_car_params
      params.require(:model_car).permit(:name, :year, :engine_power)
    end
end
