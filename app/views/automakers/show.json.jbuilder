json.resource do
  json.partial! "automakers/automaker", automaker: @automaker
end
json.message local_assigns[:message]
json.content render(partial: "automakers/show")
