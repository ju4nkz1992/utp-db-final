json.extract! automaker, :id, :name, :address, :created_at, :updated_at, :to_s
json.url automaker_url(automaker, format: :json)