json.extract! car, :id, :registration, :registration_date, :model_car_id, :owner_id, :created_at, :updated_at, :to_s
json.url car_url(car, format: :json)