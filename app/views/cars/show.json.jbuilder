json.resource do
  json.partial! "cars/car", car: @car
end
json.message local_assigns[:message]
json.content render(partial: "cars/show")