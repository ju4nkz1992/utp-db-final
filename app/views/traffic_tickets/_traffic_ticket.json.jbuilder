json.extract! traffic_ticket, :id, :owner_id, :car_id, :traffic_officer_id, :traffic_infraction_id, :city_id, :date, :address, :cost, :created_at, :updated_at
json.url traffic_ticket_url(traffic_ticket, format: :json)
