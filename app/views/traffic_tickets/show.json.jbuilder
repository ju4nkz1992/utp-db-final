json.resource do
  json.partial! "traffic_tickets/traffic_ticket", traffic_ticket: @traffic_ticket
end
json.message local_assigns[:message]
json.content render(partial: "traffic_tickets/show")
