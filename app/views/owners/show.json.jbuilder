json.resource do
  json.partial!("owners/owner", owner: @owner)
end
json.message local_assigns[:message]
json.content render(partial: "owners/show")