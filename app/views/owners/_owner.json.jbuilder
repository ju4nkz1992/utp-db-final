json.extract! owner, :id, :nit, :name, :last_name, :birthdate, :street, :street_number, :city_id, :created_at, :updated_at, :full_name, :to_s
json.url owner_url(owner, format: :json)