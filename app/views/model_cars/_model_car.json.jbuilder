json.extract! model_car, :id, :name, :year, :engine_power, :automaker_id, :created_at, :updated_at, :to_s
json.url automaker_model_car_url(model_car.automaker_id, model_car, format: :json)