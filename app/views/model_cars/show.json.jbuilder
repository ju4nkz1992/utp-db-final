json.resource do
  json.partial!("model_cars/model_car", model_car: @model_car)
end
json.message local_assigns[:message]
json.content render(partial: "model_cars/show")