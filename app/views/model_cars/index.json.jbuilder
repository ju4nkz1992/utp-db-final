json.content render(partial: 'model_cars/list')
json.resources do
  json.array! @model_cars, partial: 'model_cars/model_car', as: :model_car
end
