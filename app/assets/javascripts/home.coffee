# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

window.showSuccessAlert = (message)->
  alert = $('<div class="alert alert-info alert-dismissible" role="alert"></div>')
  alert.append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
  alert.append(message)

  $('#alerts-container').append(alert)
  setTimeout ()->
    $( alert ).fadeOut 3000, ()->
      $(this).remove()
  , 7000

ready = (evt)->
  $.fn.datepicker.defaults.format = "dd/mm/yyyy";
  container = $(evt.target)

  if container.find('form.auto-submit').length
    container.find('form.auto-submit').each ()->
      $(this).submit()

$(document).on 'change', 'form.auto-submit input, form.auto-submit select', ()->
  $(this).parents('form.auto-submit').submit()

$(document).on 'click', '.modal .modal-header .close', (evt, data)->
  $(this).parents('.modal').modal('hide')

$(document).on 'click', 'a.back', (evt)->
  evt.preventDefault()
  step = $(this).attr('step')
  prevStepContent = $(this).parents('.step-' + step).parent().find('.step-' + (step - 1))
  if step == '1' || prevStepContent.length < 1
    $('.modal').modal('hide')
  else
    prevStepContent.removeClass('hide')

  $(this).parents('.step-' + step).remove()

$(document).on 'change', 'select.department_select', ()->
  department = $(this)
  if department.val() != ''
    $.get '/departments/' + department.val() + '/cities', (data,state)->
      select = $( department.data('citySelect') )
      select.html('<option>Seleccione ciudad</option>')
      $.each data, ()->
        select.append('<option value="'+this.id+'">'+this.name+'</option>')

$(document).on 'change', 'select.fabricante_select', ()->
  fabricante = $(this)
  $('#create_model_car').attr('href', '/automakers/' + fabricante.val() + '/model_cars/new.json')
  if fabricante.val() != ''
    $.get '/automakers/' + fabricante.val() + '/model_cars.json', (data,state)->
      select = $( fabricante.data('modelSelect') )
      select.html('<option>Seleccione Modelo</option>')
      $.each data.resources, ()->
        select.append('<option value="'+this.id+'">'+this.to_s+'</option>')

$(document).on 'change', '.infraction-select', ()->
  $.get '/traffic_infractions/' + $(this).val() + '.json', (data,state)->
    $('#tipo_urban').data('price', data.resource.cost_urban_area)
    $('#tipo_rural').data('price', data.resource.cost_rural_area)

    $('#tipo_rural:checked, #tipo_urban:checked').trigger('change')

$(document).on 'change', '#tipo_rural, #tipo_urban', ()->
  $('#traffic_ticket_cost').val($(this).data('price'))

$(document).on 'ready page:load', ready
