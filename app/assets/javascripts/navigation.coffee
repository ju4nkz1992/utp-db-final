get_link_container = (link)->
  container = $(link.data('container'))
  unless container.length
    container = $('#main-results')

  return container

deleted_element = (element)->
  element.parents('tr:first').remove()

render_modal = (container, content, options, step)->
  if step == '1'
    container.find('.modal-title').text(options.title)
    container.find('.modal-body').html('')

  content.append('<a href="#" class="back btn btn-default" step="'+ step + '">Cancelar</a>')

  content = $('<div class="modal-step step-' + step + '"></div>').html(content)
  container.find('.modal-step').addClass('hide')
  container.find('.modal-body').append(content)
  container.find('.modal-body .navigation').addClass('hide')
  container.find('.modal-body .step-' + step + ' form').data('container', options.select).data('step', step)
  container.modal('show')

  container.trigger("page:load")

render_content = (container, content)->
  $('.modal').modal('hide')
  container.html(content)

  container.trigger("page:load")

append_select = (container, elem, data, step)->
  prevStepContent = elem.parents('.step-' + step).parent().find('.step-' + (step - 1))
  if step == '1' || prevStepContent.length < 1
    $('.modal').modal('hide')
  else
    prevStepContent.removeClass('hide')
    elem.parents('.step-' + step).remove()

  container.find("option:selected").removeAttr("selected");
  container.append('<option value="'+data.id+'" selected>'+data.to_s+'</option>')
  container.trigger('change')

determine_render_action = (elem, container, options, step, content, data)->
  return deleted_element(elem) if elem.hasClass('deleted')
  return append_select(container, elem, data.resource, step) if data.resource && container.is('select')
  return render_modal(container, content, options, step) if data.content && container.hasClass('modal')
  return render_content(container, content) if data.content

remove_loading = (container)->
  container = container.find('.modal-body') if container.hasClass('modal')
  container.removeClass('opaque')
  container.find('.loading-container').remove()

$(document).on 'ajax:beforeSend', 'form, a', (evt, data)->
  evt.stopPropagation()
  container = get_link_container($(this))
  if container.hasClass('modal')
    container.modal('show')
    container = container.find('.modal-body')

  container.addClass('opaque')
  container.append('<div class="loading-container"><div class="loading">' + $('#loading').html() + '</div></div>')

$(document).on 'ajax:success', 'form, a', (evt, data)->
  evt.stopPropagation()

  elem      = $(this)
  container = get_link_container(elem)
  options   = elem.data()
  step      = (options.step || '1')
  content   = $(data.content)

  showSuccessAlert(data.message) if data.message
  determine_render_action(elem, container, options, step, content, data)
  remove_loading(container)

  if elem.attr('href')
    anchor = elem.attr('href').split('#')[1]
    element = $('#' + anchor)
    if anchor && element.length > 0
      $('#accordion .panel-collapse.in').removeClass('in')
      element.addClass('in')

$(document).on 'ajax:error', 'form, a', (evt, data)->
  evt.stopPropagation()
  console.log data

  elem = $(this)
  container = get_link_container(elem)
  remove_loading(container)
