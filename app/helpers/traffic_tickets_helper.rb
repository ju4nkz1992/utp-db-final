module TrafficTicketsHelper
  def ticket_table_class(ticket)
    ticket.owner.active? ? '' : 'danger'
  end
end
