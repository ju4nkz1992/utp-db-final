#
# Tabla :departments
#
# id              :integer PK
# name            :string
# departament_id  :integer FK
# created_at:     :datetime
# update_at:      :datetime

class City < ApplicationRecord
  belongs_to :department

  def to_s
    "#{department.name}, #{name}"
  end
end
