#
# Tabla traffic_infractions
#
# id              :integer PK
# article         :string
# cost_urban_area :decimal(15,2)
# cost_rural_area :decimal(15,2)
# description     :text
# created_at:     :datetime
# update_at:      :datetime

class TrafficInfraction < ApplicationRecord
  validates :article, :cost_urban_area, :cost_rural_area, presence: true
  validates :article, uniqueness: true

  def to_s
    article
  end

  def self.to_select filter = {}
    self.where(filter).pluck(:article, :id)
  end
end
