#
# Tabla automakers
#
# id          :integer PK
# name        :string
# address     :string
# created_at: :datetime
# update_at:  :datetime

class Automaker < ApplicationRecord
  validates :name, presence: true

  has_many :model_cars

  SEARCH_ATTRIBUTES = [:name, :address]
  SEARCH_JOINS = []
  DEFAULT_INCLUDES = []

  include Search

  def to_s
    name
  end

  def self.to_select filter = {}
    self.where(filter).pluck(:name, :id)
  end
end
