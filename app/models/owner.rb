#
# Tabla owners
#
# id            :integer PK
# nit           :string
# name          :string
# last_name     :string
# birthdate     :date
# street        :string
# street_number :string
# city_id       :integer FK
# created_at:   :datetime
# update_at:    :datetime

class Owner < ApplicationRecord
  belongs_to :city
  has_many :cars

  SEARCH_ATTRIBUTES = [:nit, 'owners.name', :last_name, "owners.name || ' ' || last_name", "cities.name"]
  SEARCH_JOINS = [:city]
  DEFAULT_INCLUDES = [{city: [:department]}]

  include LogicalDestroy
  include Search

  validates :nit, :name, :last_name, :birthdate, :street, :street_number, :city_id, presence: true

  def full_name
    "#{name} #{last_name}"
  end

  def to_s
    full_name
  end

  def age
    ((Date.today() - birthdate).to_i/365.25).floor
  end

  def self.to_select filter = {}
    self.active.where(filter).pluck("name || ' ' || last_name || '(' || nit ||')'", :id)
  end

  def self.all_to_select filter = {}
    self.where(filter).pluck("name || ' ' || last_name || '(' || nit ||')'", :id)
  end
end
