#
# Tabla :departments
#
# id          :integer PK
# name        :string
# created_at: :datetime
# update_at:  :datetime
class Department < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :cities
end
