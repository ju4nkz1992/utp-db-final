#
# Tabla traffic_officers
#
# id              :integer PK
# registry_number :string
# name            :string
# created_at:     :datetime
# update_at:      :datetime

class TrafficOfficer < ApplicationRecord
  validates :registry_number, presence: true, uniqueness: true

  def to_s
    "#{name}(#{registry_number})"
  end

  def self.to_select filter = {}
    self.where(filter).pluck("name || '(' || registry_number || ')'", :id)
  end
end
