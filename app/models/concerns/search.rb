module Search
  extend ActiveSupport::Concern

  module ClassMethods
    def search text, order_by, order_tipe = :asc, filters = {}
      return all_with_order(order_by, order_tipe, filters) unless text.present?

      texts   = text.split(' ')
      queries = []

      texts.each do |text|
        or_queries = []
        self::SEARCH_ATTRIBUTES.each do |field|
          or_queries << "lower(#{field}) LIKE lower('%#{text}%')"
        end
        queries << "(#{or_queries.join(' OR ')})"
      end

      query   = queries.join(' AND ')
      _object = all_relations(self, filters)
      _object = _object.where(query)
      order_by.present? ? _object.order("#{order_by} #{order_tipe}") : _object
    end

    def all_with_order(order_by, order_tipe, filters)
      order_by = order_by.presence || "#{self.name.underscore.pluralize}.id"
      _object  = all_relations(self, filters)
      _object.order("#{order_by} #{order_tipe}")
    end

    def all_relations(_object, filters)
      _object = _object.respond_to?(:active) ? _object.active : _object
      _object = _object.where(filters) if filters.present?
      _object = _object.left_outer_joins(*self::SEARCH_JOINS) if self::SEARCH_JOINS.present?
      _object = _object.preload(*self::DEFAULT_INCLUDES) if self::DEFAULT_INCLUDES.present?
      _object
    end
  end
end
