module LogicalDestroy
  extend ActiveSupport::Concern

  def destroy
    update_attributes(archived: true, archive_at: DateTime.current)
  end

  def rebuild
    update_attributes(archived: false, archive_at: nil)
  end

  def active?
    !archived?
  end

  module ClassMethods
    def active
      where(archived: false)
    end

    def archived
      all_resources.where(archived: true)
    end

    def all_resources
      unscoped
    end
  end
end
