#
# Tabla cars
#
# id                :integer PK
# registration      :string
# registration_date :date
# model_car         :integer FK
# owner             :integer FK
# created_at:       :datetime
# update_at:        :datetime

class Car < ApplicationRecord
  REGISTRATION_REGEX = /^(\w{3})(\W|\_)*(\w{2,})\s*$/

  belongs_to :model_car
  belongs_to :owner

  before_save :parameterize_registration if :registration_changed?

  SEARCH_ATTRIBUTES = [:registration, "substring(registration from 0 for 4)", "substring(registration from 5)", "substring(registration from 0 for 4) || substring(registration from 5)", 'model_cars.name', "to_char(model_cars.year, '9999')", 'owners.last_name', 'owners.name', 'owners.nit', 'automakers.name']
  SEARCH_JOINS = [:owner, model_car: [:automaker]]
  DEFAULT_INCLUDES = [:owner, {model_car: [:automaker]}]

  include LogicalDestroy
  include Search

  validates :model_car, :owner, :registration, :registration_date, presence: true
  def to_s
    registration
  end

  def self.to_select filter = {}
    self.active.where(filter).pluck(:registration, :id)
  end

  def parameterize_registration
    if self.registration =~ REGISTRATION_REGEX
      self.registration.gsub!(REGISTRATION_REGEX, '\1-\3')
    end

    self.registration = self.registration.upcase
  end
end
