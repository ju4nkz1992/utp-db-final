#
# Tabla model_cars
#
# id           :integer PK
# name         :string
# year         :integer
# engine_power :integer
# automaker_id :integer FK
# created_at:  :datetime
# update_at:   :datetime

class ModelCar < ApplicationRecord
  validates :name, :year, :engine_power, :automaker_id, presence: true
  belongs_to :automaker

  SEARCH_ATTRIBUTES = [:name, :year, 'automaker.name', :engine_power]
  SEARCH_JOINS = [:automaker]
  DEFAULT_INCLUDES = [:automaker]

  include LogicalDestroy
  include Search

  def to_s
    "#{name}(#{year})"
  end

  def full_name
    "#{automaker.to_s}/#{to_s}"
  end

  def self.to_select filter = {}
    self.active.where(filter).pluck("name || '(' || year || ')'", :id)
  end
end
