#
# Tabla traffic_tickets
#
# id                    :integer PK
# owner_id              :integer FK
# car_id                :integer FK
# traffic_officer_id    :integer FK
# traffic_infraction_id :integer FK
# city_id               :integer FK
# date                  :date
# address               :string
# cost                  :decimal(15,2)
# created_at:           :datetime
# update_at:            :datetime

class TrafficTicket < ApplicationRecord
  belongs_to :owner
  belongs_to :car, optional: true
  belongs_to :traffic_officer
  belongs_to :traffic_infraction
  belongs_to :city

  validates :owner_id, :traffic_officer_id, :traffic_infraction_id, :city_id, presence: true

  SEARCH_ATTRIBUTES = ['owners.name', 'owners.last_name', "owners.name || ' ' || owners.last_name", :address, "cities.name", 'cars.registration', "substring(registration from 0 for 4)", "substring(registration from 5)", "substring(registration from 0 for 4) || substring(registration from 5)", 'traffic_infractions.article', 'traffic_officers.name']
  SEARCH_JOINS      = [:owner, :car, :traffic_infraction, :traffic_officer, :city]
  DEFAULT_INCLUDES  = [:owner, :car, :traffic_infraction, :traffic_officer, {city: [:department]}]

  include Search

  def full_address
    "#{self.city.try(:department).try(:name)}, #{self.city.try(:name)}, #{self.address}"
  end

  def self.to_select filter = {}
    self.where(filter).pluck(:id, :id)
  end
end
